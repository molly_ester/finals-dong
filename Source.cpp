#include "Queue.h"
#include "Stack.h"
#include <iostream>
#include <conio.h>

using namespace std;

int main() {
	int input;

	cout << "Enter size for element sets: ";
	cin >> input;
	Queue<int> queue(input);
	Stack<int> stack(input);

	cout << endl;
	
	while (1) {
		cout << "What do you want to do?\n1 - Push elements\n2 - Pop elements\n3 - Print everything then empty set\nInput: ";
		do cin >> input; while (input > 3 || input < 1);
		if (input == 1) {
			cout << "\nEnter number: ";
			cin >> input;
			cout << endl;
			
			queue.push(input);
			stack.push(input);
			
			cout << "\nTop Elements of Set:\n";
			cout << "Queue: " << queue.top() << endl;
			cout << "Stack: " << stack.top() << endl;
		}
		else if (input == 2) {
			cout << "You have popped the front elements.\n\n";
			stack.pop();
			queue.pop();

			cout << "Top Elements of Set:\n";
			cout << "Queue: " << queue.top() << endl;
			cout << "Stack: " << stack.top() << endl;
		}
		else {
			cout << "Queue Elements: ";
			for (int i = 0; i < queue.getSize(); i++) {
				cout << queue[i] << " ";
			}

			cout << "\nStack Elements: ";
			for (int i = 0; i < stack.getSize(); i++) {
				cout << stack[i] << " ";
			}

			stack.clear();
			queue.clear();
		}
		_getch();
		system("CLS");
		
	}
}