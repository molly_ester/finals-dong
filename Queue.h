#pragma once
#include "UnorderedArray.h"

template<class T>
class Queue : public UnorderedArray<T> {
public:
	Queue(int size, int growBy = 1) : UnorderedArray<T>(size){}
	
	virtual ~Queue() {
		UnorderedArray<T>::~UnorderedArray();
	}

	virtual void push(T value) {
		UnorderedArray<T>::push(value);
		this->mArray[this->mNumElements - 1] = value;
	}
};

