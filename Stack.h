#pragma once
#include "UnorderedArray.h"
template<class T>
class Stack : public UnorderedArray<T> {
public:
	Stack(int size, int growBy = 1) : UnorderedArray<T>(size) {}
	virtual ~Stack() {
		UnorderedArray<T>::~UnorderedArray();
	}

	virtual void push(T value) {
		UnorderedArray<T>::push(value);
		for (int i = this->mNumElements - 1 ; i > 0; i--)
			this->mArray[i] = this->mArray[i - 1];
		this->mArray[0] = value;
	}

};